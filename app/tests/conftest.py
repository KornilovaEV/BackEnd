import pytest
import pytest_asyncio
from httpx import AsyncClient
from motor.motor_asyncio import AsyncIOMotorClient, AsyncIOMotorDatabase

from config import Config
from create_app import create_app
from server.database import Collections


@pytest.fixture
def config() -> Config:
    return Config()


async def clean_collections(db: AsyncIOMotorDatabase) -> None:
    for coll in await db.list_collection_names():
        await db[coll].drop()

@pytest_asyncio.fixture
async def db(config: Config) -> AsyncIOMotorClient:
    db = AsyncIOMotorClient(config.mongo_url).get_database(config.db)
    await clean_collections(db)
    return db

@pytest_asyncio.fixture
async def collection(db) -> Collections:
    return Collections(db)

@pytest_asyncio.fixture()
async def client():
    app = create_app()
    async with AsyncClient(app=app, base_url='http://fetest') as client:
        yield client

