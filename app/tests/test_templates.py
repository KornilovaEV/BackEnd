from unittest.mock import patch

import pytest

from server.database import Collections


@pytest.mark.asyncio
@patch('dependencies.get_collections')
async def test_read_items(get_collections, client, collection: Collections):
    get_collections.return_value = collection
    insert_temp = [{'f1': 'f1', 'f2': {'test': 'test_f2'}}, {'f3': 'f3'}]
    await collection.templates.insert_many([{'f1': 'f1', 'f2': {'test': 'test_f2'}}, {'f3': 'f3'}])
    response = await client.get("/api/v1/templates")
    assert response.status_code == 200
    with open('kk.txt', 'w') as f:
        rr = response.json()['data']
        f.write(f'{rr}\n\n{insert_temp}')
    assert response.json()['data'] == insert_temp
        
