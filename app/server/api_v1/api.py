from fastapi import APIRouter

from server.api_v1.endpoints import templates

server_api_router = APIRouter()
server_api_router.include_router(templates.router, prefix='/templates', tags=['templates'])
