import math

from fastapi import APIRouter, Depends, Query

from dependencies import get_collections
from server.database import Collections

router = APIRouter()


@router.get("", response_model=dict, response_description="Templates list")
async def get_templates(
    collections: Collections = Depends(get_collections),
    page: int = Query(default=1, title='Номер страницы'),
    size: int = Query(default=25, title='Количество элементов на странице')
):
    return {
        "page": page,
        "size": size,
        "total": math.ceil(await collections.templates.count_documents({}) / size),
        "data": [item async for item in collections.templates.find({}, {'_id': 0}).skip((page - 1) * size).limit(size)]
    }
