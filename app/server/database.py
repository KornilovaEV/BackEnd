from motor.motor_asyncio import AsyncIOMotorClient


class Collections:
    db: AsyncIOMotorClient

    def __init__(self, db) -> None:
        self.db = db
        self.templates = self.db["templates"]
