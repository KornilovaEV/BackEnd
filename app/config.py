from pydantic import BaseSettings


class Config(BaseSettings):
    mongo_url: str
    db: str
    debug: bool = False

    class Config:
        # env_file = 'config/.env'
        env_file = 'config/.prod.env'


def get_config() -> Config:
    return Config()


config = get_config()
