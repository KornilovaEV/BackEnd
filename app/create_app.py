from fastapi import FastAPI

from config import config
from server.api_v1.api import server_api_router


def create_app() -> FastAPI:
    app_server = FastAPI(title=f'Back end (Server API)', debug=config.debug)
    app_server.include_router(server_api_router)

    app = FastAPI()
    app.mount('/api/v1', app_server)
    return app
