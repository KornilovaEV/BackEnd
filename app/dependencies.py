from motor.motor_asyncio import AsyncIOMotorClient

from config import config
from server.database import Collections


def get_db() -> AsyncIOMotorClient:
    return AsyncIOMotorClient(config.mongo_url).get_database(config.db)

def get_collections() -> Collections:
    return Collections(get_db())