# TestTask

## Запуск API

```cmd
docker-compose build
docker-compose up -d
```

## Запуск тестов

```cmd
docker-compose exec app pytest .
```
